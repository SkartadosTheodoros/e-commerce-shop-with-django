import React from 'react';

const Star = (rating, limit) => {
  if (+rating >= +limit) {
    return 'fas fa-star';
  } else if (+rating >= +limit - 0.5) {
    return 'fas fa-star-half-alt';
  } else {
    return 'far fa-star';
  }
};

const Rating = ({ rating, reviews, color }) => {
  return (
    <div className="rating">
      <span>
        <i style={{ color: color }} className={Star(rating, 1)}></i>
      </span>

      <span>
        <i style={{ color: color }} className={Star(rating, 2)}></i>
      </span>

      <span>
        <i style={{ color: color }} className={Star(rating, 3)}></i>
      </span>

      <span>
        <i style={{ color: color }} className={Star(rating, 4)}></i>
      </span>

      <span>
        <i style={{ color: color }} className={Star(rating, 5)}></i>
      </span>

      {reviews && <span> from {reviews} reviews</span>}
    </div>
  );
};

export default Rating;
