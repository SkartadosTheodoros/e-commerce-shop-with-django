import React, { useEffect } from 'react';
import { Link, useParams, useLocation, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Row, Col, Button, ListGroup, Image, Card } from 'react-bootstrap';
import { addToCart, removeFromCart } from '../actions/cart.actions';
import Message from '../components/Message';
import Quantity from '../components/Quantity';

const CartPage = () => {
  const { id } = useParams();
  const { search } = useLocation();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const cart = useSelector((state) => state.cart);
  const quantity = search ? Number(search.split('=')[1]) : 1;

  const totalProducts = cart.cartItems.reduce((acc, item) => acc + item.quantity, 0);

  const totalProductsCost = cart.cartItems
    .reduce((acc, item) => acc + item.quantity * item.price, 0)
    .toFixed(2);

  useEffect(() => {
    if (id) {
      dispatch(addToCart(id, quantity));
    }
  }, [dispatch, id, quantity]);

  const checkoutHandler = () => {
    navigate('/login?redirect=shipping');
  };

  const removeFromCartHandler = (id) => {
    dispatch(removeFromCart(id));
  };

  return (
    <Row>
      <Col md={8}>
        <h1>Shopping Cart</h1>

        {cart.cartItems.length === 0 ? (
          <Message>
            Your cart is empty <Link to="/">Go back</Link>
          </Message>
        ) : (
          <ListGroup variant="flush">
            {cart.cartItems.map((cartItem) => (
              <ListGroup.Item key={cartItem.id}>
                <Row>
                  <Col md={2}>
                    <Image src={cartItem.image} alt={cartItem.name} rounded fluid />
                  </Col>

                  <Col md={4}>
                    <Link to={`/product/${cartItem.id}`}>{cartItem.name}</Link>
                  </Col>

                  <Col md={2}>{cartItem.price}</Col>

                  <Col md={2}>
                    <Quantity
                      stock={cartItem.countInStock}
                      value={cartItem.quantity}
                      onChange={(e) => dispatch(addToCart(cartItem.id, Number(e.target.value)))}
                    />
                  </Col>

                  <Col md={2}>
                    <Button
                      type="button"
                      variant="light"
                      onClick={removeFromCartHandler.bind(null, cartItem.id)}
                    >
                      <i className="fas fa-trash" />
                    </Button>
                  </Col>
                </Row>
              </ListGroup.Item>
            ))}
          </ListGroup>
        )}
      </Col>

      <Col md={4}>
        <Card>
          <ListGroup variant="flush">
            <ListGroup.Item>
              <h2>Subtotal ({totalProducts}) items</h2>
              <div>Total amount : {totalProductsCost} &euro;</div>
            </ListGroup.Item>

            <ListGroup.Item>
              <Button
                type="button"
                className="btn btn-block"
                disabled={cart.cartItems.length === 0}
                onClick={checkoutHandler}
              >
                Procced To Checkout
              </Button>
            </ListGroup.Item>
          </ListGroup>
        </Card>
      </Col>
    </Row>
  );
};

export default CartPage;
