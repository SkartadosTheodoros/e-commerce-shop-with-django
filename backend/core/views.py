from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import Product
from .serializer import ProductSerializer

@api_view(['GET'])
def getRoutes(request):
  return Response({"message": "Hello, world!"})

@api_view(['GET'])
def getProducts(request):
  product =  Product.objects.all()
  serializer = ProductSerializer(product, many=True)
  return Response(serializer.data)

@api_view(['GET'])
def getProduct(request, pk):
  product = Product.objects.get(_id=pk)
  serializer = ProductSerializer(product, many=False)
  return Response(serializer.data)